locals {
  common_tags = {
    owner       = var.student_name
    environment = var.environment
    project     = "Findmore Academy GitLab CI/CD"
    managed-by  = "Terraform"
  }
}