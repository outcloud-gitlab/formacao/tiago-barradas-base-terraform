output "primary_blob_connection_string" {
  description = "Primary blob connection string of the Storage Account"
  value       = azurerm_storage_account.this.primary_blob_connection_string
  sensitive   = true
}

output "app_url" {
  description = "URL of the react app created in azure"
  value       = "https://${azurerm_storage_account.this.name}.z6.web.core.windows.net/"
}